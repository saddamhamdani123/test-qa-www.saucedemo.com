<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_continue_shopping</name>
   <tag></tag>
   <elementGuidId>4fbfaba4-bdee-44d1-a656-eb135fe1267c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='continue-shopping']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#continue-shopping</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9a4202c5-0b30-47dd-8eb9-43b70432fc9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn_secondary back btn_medium</value>
      <webElementGuid>739b2372-4bb6-4898-a6a4-eb135d0ab495</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>continue-shopping</value>
      <webElementGuid>8747946d-9147-46de-9cf0-8798c3aef8dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>continue-shopping</value>
      <webElementGuid>6c8e8e65-0fbb-4530-a99f-e7007ad3b8b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>continue-shopping</value>
      <webElementGuid>ade2e6f3-a81d-4760-8773-9c5f55d00929</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Continue Shopping</value>
      <webElementGuid>5dde6b60-f27a-49d9-8669-0f3aa6610a9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;continue-shopping&quot;)</value>
      <webElementGuid>a2cdba00-def3-450f-ae01-d6844c6702ae</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='continue-shopping']</value>
      <webElementGuid>11b5aba9-c0a5-4dd7-8a58-509fb5dfdedc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='cart_contents_container']/div/div[2]/button</value>
      <webElementGuid>6b90341a-fa26-43f1-bc92-37ce4b9401bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove'])[1]/following::button[1]</value>
      <webElementGuid>24aa5ea6-5877-4653-a6a7-e24b215a619f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$29.99'])[1]/following::button[2]</value>
      <webElementGuid>e02a984b-64b3-4c8f-a892-8be55a602df8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[1]/preceding::button[1]</value>
      <webElementGuid>bcc2e8c8-89de-4895-8be9-3b755b489bcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Twitter'])[1]/preceding::button[2]</value>
      <webElementGuid>2e123aa1-3ec8-4a90-8a6f-d38063ec48c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Continue Shopping']/parent::*</value>
      <webElementGuid>789d3329-9f94-492a-981f-045c41abdc83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>25393b0c-258d-4a65-a354-486843aeb669</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'continue-shopping' and @name = 'continue-shopping' and (text() = 'Continue Shopping' or . = 'Continue Shopping')]</value>
      <webElementGuid>686b8f6d-1d27-4847-89f1-9c9330cf90a5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
